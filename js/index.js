//DOM ID
function domID(id) {
  return document.getElementById(id);
}

// Bài 1: Cho người dùng nhập vào 3 số nguyên. Viết chương trình xuất 3 số theo thứ tự tăng dần

domID("sapXep").onclick = function () {
  //input
  var min = domID("numberFirst").value;
  var mid = domID("numberSecond").value;
  var max = domID("numberThird").value;

  if (
    isNaN(min) ||
    isNaN(mid) ||
    isNaN(max) ||
    min == "" ||
    mid == "" ||
    max == ""
  ) {
    alert("Vui lòng Nhập vào các số bất kỳ!");
    return false;
  } else {
    var minValue = min * 1;
    var midValue = mid * 1;
    var maxValue = max * 1;

    //output
    //var result = domID("result");
    if (minValue <= midValue) {
      if (midValue <= maxValue) {
        xuatKetqua(minValue, midValue, maxValue);
      } else if (minValue <= maxValue) {
        xuatKetqua(minValue, maxValue, midValue);
      } else {
        xuatKetqua(maxValue, minValue, midValue);
      }
    } else {
      if (minValue <= maxValue) {
        xuatKetqua(midValue, minValue, maxValue);
      } else if (midValue <= maxValue) {
        xuatKetqua(midValue, maxValue, minValue);
      } else {
        xuatKetqua(maxValue, midValue, minValue);
      }
    }
    return true;
  }
};
//Xuat Ket qua so sanh
function xuatKetqua(min, mid, max) {
  return (domID("result").innerHTML =
    "Kết quả: " + min + "<" + mid + "<" + max);
}

// Bài 2: Viết chương trình “Chào hỏi” các thành viên trong gia đình với các đặc điểm. Đầu tiên máy sẽ hỏi ai sử dụng máy. Sau đó dựa vào câu trả lời và đưa ra lời chào phù hợp. Giả sử trong gia đình có 4 thành viên: Bố (B), Mẹ (M), anh Trai (A) và Em gái (E). Nếu không chọn => Người lạ!

domID("guiLoiChao").onclick = function () {
  //input
  var thanhvien = domID("chonThanhVien").value;
  //output
  // var result = "";
  //progress
  switch (thanhvien) {
    case "B":
      domID("xinChao").innerHTML = "Xin chào Bố!";
      break;
    case "M":
      domID("xinChao").innerHTML = "Xin chào Mẹ!";
      break;
    case "A":
      domID("xinChao").innerHTML = "Xin chào Anh trai!";
      break;
    case "E":
      domID("xinChao").innerHTML = "Xin chào Em gái!";
      break;
    default:
      domID("xinChao").innerHTML = "Xin chào Người lạ Ơi!";
  }
};

// Bài 3: Cho 3 số nguyên. Viết chương trình xuất ra có bao nhiêu số lẻ và bao nhiêu số chẵn.
domID("demChanLe").onclick = function () {
  //input
  var numberOne = domID("numberOne").value;
  var numberTwo = domID("numberTwo").value;
  var numberThree = domID("numberThree").value;
  if (
    isNaN(numberOne) ||
    isNaN(numberTwo) ||
    isNaN(numberThree) ||
    numberOne == "" ||
    numberTwo == "" ||
    numberThree == ""
  ) {
    alert("Vui lòng Nhập vào các số bất kỳ!");
    return false;
  } else {
    //output
    var soChan = 0;
    var soLe = 0;
    if (numberOne % 2 == 0) {
      soChan++;
    } else {
      soLe++;
    }
    if (numberTwo % 2 == 0) {
      soChan++;
    } else {
      soLe++;
    }
    if (numberThree % 2 == 0) {
      soChan++;
    } else {
      soLe++;
    }
    domID(
      "ketQuaDem"
    ).innerHTML = `Kết quả: Có ${soChan} số Chẵn và ${soLe} số Lẻ.`;
    return true;
  }
};

// Bài 4: Viết chương trình cho nhập 3 cạnh của tam giác. Hãy cho biết đó là tam giác gì?
domID("tamGiac").onclick = function () {
  //input
  var canhA = domID("canhA").value;
  var canhB = domID("canhB").value;
  var canhC = domID("canhC").value;
  if (
    isNaN(canhA) ||
    isNaN(canhB) ||
    isNaN(canhC) ||
    canhA <= 0 ||
    canhB <= 0 ||
    canhC <= 0
  ) {
    alert("Vui lòng Nhập vào độ dài các cạnh là các số lớn hơn 0!");
    return false;
  } else {
    canhA *= 1;
    canhB *= 1;
    canhC *= 1;
    //output
    var result = domID("ketQuaTG");

    //progress
    if (
      canhA + canhB < canhC ||
      canhA + canhC < canhB ||
      canhC + canhB < canhA
    ) {
      result.innerHTML = "Kết quả: Đây không phải là tam giác!";
    } else {
      if (canhA == canhB && canhB == canhC) {
        result.innerHTML = "Kết quả: Đây là tam giác Đều!";
      } else if (canhA == canhB || canhA == canhC || canhB == canhC) {
        // if (
        //   canhA * canhA + canhB * canhB == (canhC * canhC).toFixed(2) ||
        //   canhA * canhA + canhC * canhC == (canhB * canhB).toFixed(2) ||
        //   canhC * canhC + canhB * canhB == (canhA * canhA).toFixed(2)
        // ) {
        //   result.innerHTML = "Kết quả: Đây là tam giác Vuông Cân!";
        // } else {
        result.innerHTML = "Kết quả: Đây là tam giác Cân!";
        // }
      } else if (
        canhA * canhA + canhB * canhB == canhC * canhC ||
        canhA * canhA + canhC * canhC == canhB * canhB ||
        canhC * canhC + canhB * canhB == canhA * canhA
      ) {
        result.innerHTML = "Kết quả: Đây là tam giác Vuông!";
      } else {
        result.innerHTML = "Kết quả: Đây là Tam giác thường!";
      }
    }
    return true;
  }
};

///////// BT5: Viết chương trình tính điểm thi đại học khối A và C
domID("tinhDiem").onclick = function () {
  //Tinh diem TB khoi A
  //input
  var diemToan = domID("diemToan").value * 1;
  var diemLy = domID("diemLy").value * 1;
  var diemHoa = domID("diemHoa").value * 1;
  //output
  var diemTBKA = tinhDiemTB(diemToan, diemLy, diemHoa);

  //Hien thi ra giao dien
  domID("diemTrungBinhKhoiA").value = diemTBKA;

  //Tinh diem TB khoi C
  //input
  var diemVan = domID("diemVan").value * 1;
  var diemSu = domID("diemSu").value * 1;
  var diemDia = domID("diemDia").value * 1;

  //output
  var diemTBKC = tinhDiemTB(diemVan, diemSu, diemDia);
  //Hien thi ra giao dien
  domID("diemTrungBinhKhoiC").value = diemTBKC;
};

function tinhDiemTB(diem1, diem2, diem3) {
  //input
  if (
    isNaN(diem1) ||
    isNaN(diem2) ||
    isNaN(diem3) ||
    (diem1 < 0 && diem1 > 10) ||
    (diem2 < 0 && diem2 > 10) ||
    (diem3 < 0 && diem3 > 10)
  ) {
    alert(
      "Nhập vào điểm các môn có giá trị từ 0 đến 10. Nếu Không nhập điểm mặc định bằng 0"
    );
    return "0.00";
  } else {
    var diemTB = 0;
    diemTB = ((diem1 + diem2 + diem3) / 3).toFixed(2);

    // output
    return diemTB;
  }
}

// ///////// BT6: Viết chương trình nhập vào ngày, tháng, năm (Giả sử nhập đúng, không cần kiểm tra hợp lệ).Tìm ngày, tháng, năm của ngày tiếp theo và ngày trước đó.
//Cách 1:
//input

domID("ngayHomQuaC1").onclick = function () {
  tinhNgay(-1, "resultC1");
};
domID("ngayMaiC1").onclick = function () {
  tinhNgay(1, "resultC1");
};

//function
function tinhNgay(day, result) {
  var ngayThangNam = domID("ngayThangNam").value;
  ngayThangNam = new Date(ngayThangNam).getTime();
  const ONEDAY = 86400000;
  var day = day * ONEDAY;

  var tinhNgay = new Date(ngayThangNam + day);
  // console.log("tinhNgay: ", tinhNgay.toLocaleDateString());

  var resultC1 = domID("resultC1");
  resultC1.innerHTML = `Ngày cần tìm là: ${formatDate(
    tinhNgay.toLocaleDateString()
  )}`;
}

//format Date
function formatDate(input) {
  var datePart = input.split("/"),
    year = datePart[2],
    month = datePart[0],
    day = datePart[1];

  return day + "/" + month + "/" + year;
}

//ouput

//progress

//Cách 2:
domID("ngayHomQuaC2").onclick = function () {
  tinhNgayThangNam(-1);
};
domID("ngayMaiC2").onclick = function () {
  tinhNgayThangNam(1);
};

function tinhNgayThangNam(date) {
  //input
  var soNgay = domID("soNgay").value * 1;
  var soThang = domID("soThang").value * 1;
  var soNam = domID("soNam").value * 1;
  //progress: giả sử nhập đúng ngày tháng năm, thang 2: 28 ngay
  switch (soThang) {
    case 2:
      if (soNgay > 1 && soNgay < 28) {
        if (date == 1) {
          soNgay += 1;
        } else if (date == -1) {
          soNgay -= 1;
        } else {
          soNgay = soThang = soNam = 0;
        }
      } else if (soNgay == 1) {
        if (date == 1) {
          soNgay += 1;
        } else if (date == -1) {
          soNgay = 31;
          soThang = 1;
        } else {
          soNgay = soThang = soNam = 0;
        }
      } else if (soNgay == 28) {
        if (date == 1) {
          soNgay = 1;
          soThang = 3;
        } else if (date == -1) {
          soNgay -= 1;
        } else {
          soNgay = soThang = soNam = 0;
        }
      } else {
        soNgay = soThang = soNam = 0;
      }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      // maxNgay = 30;
      if (soNgay > 1 && soNgay < 30) {
        if (date == 1) {
          soNgay += 1;
        } else if (date == -1) {
          soNgay -= 1;
        } else {
          soNgay = soThang = soNam = 0;
        }
      } else if (soNgay == 1) {
        if (date == 1) {
          soNgay += 1;
        } else if (date == -1) {
          soNgay = 31;
          soThang -= 1;
        } else {
          soNgay = soThang = soNam = 0;
        }
      } else if (soNgay == 30) {
        if (date == 1) {
          soNgay = 1;
          soThang += 1;
        } else if (date == -1) {
          soNgay -= 1;
        } else {
          soNgay = soThang = soNam = 0;
        }
      } else {
        soNgay = soThang = soNam = 0;
      }
      break;
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      // maxNgay = 31;
      if (soNgay > 1 && soNgay < 31) {
        if (date == 1) {
          soNgay += 1;
        } else if (date == -1) {
          soNgay -= 1;
        } else {
          soNgay = soThang = soNam = 0;
        }
      } else if (soNgay == 1) {
        if (date == 1) {
          soNgay += 1;
        } else if (date == -1) {
          //ngay 1/1
          soNgay = 31;
          if (soThang == 1) {
            soThang = 12;
            soNam -= 1;
          } else if (soThang == 3) {
            soNgay = 28;
            soThang -= 1;
          } else {
            soThang -= 1;
          }
        }
      } else if (soNgay == 31) {
        if (date == 1) {
          soNgay = 1;
          if (soThang == 12) {
            soThang = 1;
            soNam += 1;
          } else {
            soThang += 1;
          }
        } else if (date == -1) {
          soNgay -= 1;
        } else {
          soNgay = soThang = soNam = 0;
        }
      } else {
        soNgay = soThang = soNam = 0;
      }
      break;
    default:
      maxNgay = 0;
  }

  // output
  domID("resultC2").innerHTML = `Ngày cần tìm là: ${
    soNgay + "/" + soThang + "/" + soNam
  }`;
}

////Bài 7: Viết chương trình tính số ngày

domID("tinhSoNgay").onclick = function () {
  //input
  var soThang = domID("months").value * 1;
  var soNam = domID("years").value * 1;

  //output
  var soNgay = 0;

  //progress
  switch (soThang) {
    case 2:
      if ((soNam % 4 == 0 && soNam % 100 != 0) || soNam % 400 == 0) {
        soNgay = 29;
      } else {
        soNgay = 28;
      }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      // maxNgay = 30;
      soNgay = 30;
      break;
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      // maxNgay = 31;
      soNgay = 31;
      break;
    default:
      soNgay = 0;
  }
  // console.log("soNgay: ", soNgay);

  domID("resultDay").innerHTML =
    "Kết quả: Tháng " + soThang + " Năm " + soNam + " có: " + soNgay + " Ngày";
};

//Bài 8: Đọc số có 3 chữ số
domID("docSo").onclick = function () {
  // input
  var soCanDoc = domID("soCanDoc").value * 1;

  //output
  var resultDocSo = "";

  //progress
  var hangTram = Math.floor(soCanDoc / 100);
  var hangChuc = Math.floor((soCanDoc % 100) / 10);
  var hangDonVi = Math.floor(soCanDoc % 10);

  //bien doc chu
  var chuHangTram = "";
  var chuHangChuc = "";
  var chuHangDonVi = "";

  if (hangTram == 0) {
    if (hangChuc == 0) {
      if (hangDonVi == 0) {
        chuHangDonVi = "Không";
      } else {
        chuHangDonVi = kiemTraSo(hangDonVi);
      }
    } else if (hangChuc == 1) {
      chuHangChuc = "Mười ";
      if (hangDonVi == 0) {
        chuHangDonVi = "";
      } else {
        chuHangDonVi = kiemTraSo(hangDonVi);
      }
    } else {
      chuHangChuc = kiemTraSo(hangChuc) + " mươi";
      if (hangDonVi == 0) {
        chuHangDonVi = "";
      } else {
        chuHangDonVi = kiemTraSo(hangDonVi);
      }
    }
    resultDocSo = chuHangChuc + " " + chuHangDonVi;
  } else {
    chuHangTram = kiemTraSo(hangTram) + " trăm";
    if (hangChuc == 0) {
      if (hangDonVi == 0) {
        chuHangChuc = "";
        chuHangDonVi = "";
      } else {
        chuHangChuc = " lẻ ";
        chuHangDonVi = kiemTraSo(hangDonVi);
      }
    } else if (hangChuc == 1) {
      chuHangChuc = "Mười ";
      if (hangDonVi == 0) {
        chuHangDonVi = "";
      } else {
        chuHangDonVi = kiemTraSo(hangDonVi);
      }
    } else {
      chuHangChuc = kiemTraSo(hangChuc) + " mươi";
      if (hangDonVi == 0) {
        chuHangDonVi = "";
      } else {
        chuHangDonVi = kiemTraSo(hangDonVi);
      }
    }
    resultDocSo = chuHangTram + " " + chuHangChuc + " " + chuHangDonVi;
  }
  domID("resultDocSo").innerHTML = `Kết quả: ${resultDocSo}`;
};

function kiemTraSo(so) {
  var chuSo = "";
  switch (so) {
    case 1:
      chuSo = "Một";
      break;
    case 2:
      chuSo = "Hai";
      break;
    case 3:
      chuSo = "Ba";
      break;
    case 4:
      chuSo = "Bốn";
      break;
    case 5:
      chuSo = "Năm";
      break;
    case 6:
      chuSo = "Sáu";
      break;
    case 7:
      chuSo = "Bảy";
      break;
    case 8:
      chuSo = "Tám";
      break;
    case 9:
      chuSo = "Chín";
      break;
    default:
      chuSo = "";
  }
  return chuSo;
}

//Bài 9: Viết chương trình Tìm sinh viên xa trường nhất!
domID("timSV").onclick = function () {
  // // input
  //SV 1
  var tenSV1 = domID("tenSV1").value;
  var toaDoX1 = domID("toaDoX1").value;
  var toaDoY1 = domID("toaDoY1").value;

  //SV 2
  var tenSV2 = domID("tenSV2").value;
  var toaDoX2 = domID("toaDoX2").value;
  var toaDoY2 = domID("toaDoY2").value;

  //SV 3
  var tenSV3 = domID("tenSV3").value;
  var toaDoX3 = domID("toaDoX3").value;
  var toaDoY3 = domID("toaDoY3").value;

  //Truong
  var toaDoX = domID("toaDoX").value;
  var toaDoY = domID("toaDoY").value;

  //output
  var khoangCachSV1 = tinhKhoangCachOxy(toaDoX1, toaDoY1, toaDoX, toaDoY);
  var khoangCachSV2 = tinhKhoangCachOxy(toaDoX2, toaDoY2, toaDoX, toaDoY);
  var khoangCachSV3 = tinhKhoangCachOxy(toaDoX3, toaDoY3, toaDoX, toaDoY);

  //progress
  var ketQuaKC = soSanhD(
    khoangCachSV1,
    khoangCachSV2,
    khoangCachSV3,
    tenSV1,
    tenSV2,
    tenSV3
  );
  domID("ketQuaKC").innerHTML = ketQuaKC;
};
// tính khoảng cách
function tinhKhoangCachOxy(x1, y1, x, y) {
  var khoangCach = Math.sqrt((x1 - x) * (x1 - x) + (y1 - y) * (y1 - y));
  return khoangCach;
}
// so sánh tìm khoảng cách lớn nhất

function soSanhD(d1, d2, d3, tenSV1, tenSV2, tenSV3) {
  var noiDung = "";
  if (d1 == d2 && d1 == d3) {
    // d1=d2=d3
    noiDung = `Cả 3 Sinh viên ${
      tenSV1 + " " + tenSV2 + " " + tenSV3
    } có Khoảng cách so với Trường bằng nhau!`;
  } else if (d1 == d2) {
    if (d1 > d3) {
      // d1=d2>d3
      noiDung = `Sinh viên ${tenSV1} và Sinh viên ${tenSV2} xa Trường nhất`;
    } else {
      //d3>d2=d1
      noiDung = `Sinh viên ${tenSV3} xa Trường nhất`;
    }
  } else if (d1 == d3) {
    if (d1 > d2) {
      // d1=d3>d2
      noiDung = `Sinh viên ${tenSV1} và Sinh viên ${tenSV3} xa Trường nhất`;
    } else {
      //d2>d1=d3
      noiDung = `Sinh viên ${tenSV2} xa Trường nhất`;
    }
  } else if (d2 == d3) {
    if (d2 > d1) {
      // d2=d3>d1
      noiDung = `Sinh viên ${tenSV2} và Sinh viên ${tenSV3} xa Trường nhất`;
    } else {
      //d1>d2=d3
      noiDung = `Sinh viên ${tenSV1} xa Trường nhất`;
    }
  } else if (d1 > d2) {
    if (d1 > d3) {
      //d1>d3 va d1>d2 =>d1
      noiDung = `Sinh viên ${tenSV1} xa Trường nhất`;
    } else {
      //d1>d2 va d1<d3 =>d3
      noiDung = `Sinh viên ${tenSV3} xa Trường nhất`;
    }
  } else if (d1 < d2) {
    if (d1 > d3) {
      //d2>d1>d3
      noiDung = `Sinh viên ${tenSV2} xa Trường nhất`;
    } else {
      //d1<d3
      if (d2 > d3) {
        //d2>d3>d1
        noiDung = `Sinh viên ${tenSV2} xa Trường nhất`;
      } else {
        // d3>d2 va d3>d1 =>d3
        noiDung = `Sinh viên ${tenSV3} xa Trường nhất`;
      }
    }
  }
  return noiDung;
}
